# systax=docker/dockerfile:1
FROM openjdk:18-alpine
COPY target/docker-test-0.0.1-SNAPSHOT.jar app.jar
CMD ["java", "-jar", "app.jar"]